
var data;

fetch('./vocaboli.json')
  .then(res => res.json())
  .then(resData => data = resData)
  .then(() => {

let max = data.length 
let number = getRandomInt(max)


let question = data[number].ITA
let correctAnswer = data[number].ENG
document.querySelector('#question').innerHTML= question

let insulti = ["Non te la prendere se c’è chi ti considera mezzo scemo. Si vede che ti conosce solo a metà…","La natura non è sempre perfetta… ma con te ha davvero esagerato!","Non ti sputo in faccia per rispetto della mia saliva.","Per favore, non interrompermi mentre ti sto ignorando.","Mi piaci così tanto che ti applaudirei per ore… con la tua testa in mezzo e due mattoni in mano.","Tu non sei una perla rara. Sei un pirla raro!","Io non ti ho insultato. Ti ho descritto!","Sei simpatico come la forfora che ti porti in testa.","Non è vero che sei inutile. Servi da cattivo esempio.","Tengo una tua foto sempre con me… così ho risolto i miei problemi di stitichezza.","Sei fortunata! Non hai bisogno di un asciugamano per la faccia e uno per il sedere. Per te va bene lo stesso.","Il mondo fa schifo. Non per niente tu sei un uomo di mondo.","Se trovi difficile ridere di te stesso, sarei felice di farlo per te.","Sai cosa ti starebbe bene addosso? Un camion.","Provo qualcosa per te: lo schifo!","La vita mi ha insegnato che alcune persone bisogna saperle prendere… a calci nel sedere!","Sei il nulla mischiato col niente.","Amo le persone che parlano alle mie spalle… hanno capito qual è il loro posto… dietro di me!","Sei talmente brutta che faresti ribrezzo al mostro di Lochness.","Sei così brutto che se una ragazza ti chiede di uscire è perché ti sei chiuso in bagno.","E se un giorno cadrai, io sarò lì a prenderti… per il culo.","Più ti guardo, più mi sale l’autostima!","La vita è breve… non posso sprecarla a sentire le tue stronzate.","Ti devo proprio ringraziare. Appena ti ho visto mi è passato il singhiozzo!","Ho ancora molto da darti. Fuoco, per esempio.","Porti così tanta sfortuna che un gatto nero quando ti vede tocca ferro.","Tutti sempre di fretta, ma il tempo per rompere le scatole lo trovano sempre!","Sei cosi brutto che quando sei nato tua madre ha mandato i bigliettini di scusa a tutti.","Non prendertela con i tuoi genitori per come sei fatto… poverini, anche loro ci saranno rimasti molto male.","Non dico che ti odio, ma se ti facessi male e io avessi in mano un telefono, lo userei per ordinare una pizza.","Hai un naso talmente grosso che se fai il modello ad un pittore il quadro esce col manico.","La cravatta è un accessorio maschile che serve ad indicare dove si trova il cervello degli uomini.","Tu ai miei livelli? Non ci arriveresti neanche con l’ascensore, rassegnati!","Sei fastidioso come l’olio che nonostante lo si tenti di lavarlo via rimane sempre li e ti scivola addosso… proprio come te.","Sei sull’orlo dell’abisso, ma presto farai un passo in avanti!","Non è che ti odio, ma siccome devo difendere l’ambiente se vedo spazzatura per terra ti dovrei raccogliere! Sai c’è crisi, e di pagare una multa non mi va!","Certo che se la luce dipendesse da te sarebbe sempre buio… e mollami!"]
let maxInsultui= insulti.length
let numberInsulti = getRandomInt(maxInsultui)

var button = document.querySelector('#confirm')
button.addEventListener('click', ()=>{

    let answer = document.querySelector('#answer').value

    if(answer.toLocaleLowerCase() == correctAnswer.toLocaleLowerCase()){
        document.querySelector('#error').classList.add('d-none')
        document.querySelector('#correct').classList.remove('d-none')
        document.querySelector('#next').classList.remove('d-none')


    }else{
        document.querySelector('#correct').classList.add('d-none')
        document.querySelector('#error').classList.remove('d-none')
        document.querySelector('#messageError').innerHTML = insulti[numberInsulti]
        document.querySelector('#rispostaEsatta').innerHTML = correctAnswer
        document.querySelector('#next').classList.remove('d-none')

    }
})



})




function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}


